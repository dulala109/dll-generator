package com.hcs.generator.controller;

import com.hcs.generator.service.SysGeneratorService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * @Author: chaoshen
 */
@Controller
@RequestMapping
@Slf4j
public class SysGeneratorController {

    @Autowired
    private SysGeneratorService sysGeneratorService;

    @GetMapping("")
    public String index() {
        return "index";
    }

    /**
     * 列表
     * http://127.0.0.1:8000/generator/list?page=0&limit=10
     */
    @ResponseBody
    @GetMapping("/list")
    public Map<String, Object> getTableList(@RequestParam Map<String, Object> params) {
        log.info("查询列表");
        return sysGeneratorService.queryList(params);
    }

    /**
     * 生成代码FileUtil
     * http://127.0.0.1:8000/generator/code?tables=sea_home_service,sea_home_service_log,sea_home_service_relation
     */
    @GetMapping("/code")
    public void makeCode(String tables, HttpServletResponse response) throws IOException {
        log.info("开始下载：{}",tables);
        byte[] data = sysGeneratorService.generatorCode(tables.split(","));
        response.reset();
        response.setHeader("Content-Disposition", "attachment; filename=\"hcs-generator.zip\"");
        response.addHeader("Content-Length", "" + data.length);
        response.setContentType("application/octet-stream; charset=UTF-8");
        IOUtils.write(data, response.getOutputStream());
    }

}
