package com.hcs.generator.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hcs.generator.mapper.SysGeneratorMapper;
import com.hcs.generator.service.SysGeneratorService;
import com.hcs.generator.utils.GenUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * @Author zlt
 */
@Slf4j
@Service
public class SysGeneratorServiceImpl extends ServiceImpl implements SysGeneratorService {

    @Autowired
    private SysGeneratorMapper sysGeneratorMapper;

    @Override
    public Map<String, Object> queryList(Map<String, Object> map) {
        Page<Map<String, Object>> page = new Page<>(MapUtils.getInteger(map, "page"), MapUtils.getInteger(map, "limit"));
        Map<String, Object> mapp = new HashMap<>();
        try {
            List<Map<String, Object>> list = sysGeneratorMapper.queryList(page, map);
            int size = sysGeneratorMapper.queryTotal(map);
            mapp.put("code", 200);
            mapp.put("msg", "获取信息成功");
            mapp.put("count",size);
            mapp.put("data", list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            mapp.put("code", 500);
            mapp.put("msg", "信息查询异常");
        }
        return mapp;
    }

    @Override
    public Map<String, String> queryTable(String tableName) {
        return sysGeneratorMapper.queryTable(tableName);
    }

    @Override
    public List<Map<String, String>> queryColumns(String tableName) {
        return sysGeneratorMapper.queryColumns(tableName);
    }

    @Override
    public byte[] generatorCode(String[] tableNames) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try (
                ZipOutputStream zip = new ZipOutputStream(outputStream)
        ) {
            for (String tableName : tableNames) {
                //查询表信息
                Map<String, String> table = queryTable(tableName);
                //查询列信息
                List<Map<String, String>> columns = queryColumns(tableName);
                //生成代码
                GenUtils.generatorCode(table, columns, zip);
            }
        } catch (IOException e) {
            log.error("generatorCode-error: ", e);
        }
        return outputStream.toByteArray();
    }
}
