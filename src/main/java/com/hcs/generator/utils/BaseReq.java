package com.hcs.generator.utils;

import lombok.Data;

import java.io.Serializable;

/**
 * @author chaoshen
 * @data 2020/5/27 0027
 */
@Data
public class BaseReq implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 分页 页码
     */
    private Integer pageNo = 1;

    /**
     * 分页 页行
     */
    private Integer pageSize = 10;

    /**
     * 模糊查询字段
     */
    private String keywords;

}