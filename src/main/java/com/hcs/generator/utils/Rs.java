package com.hcs.generator.utils;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * 返回数据的包装类
 * message 是 message，data 是 data，请区分清楚
 * fail 代表的是 400，error 代表的是 500
 * 原则上 error 仅仅在全局异常捕获了位置异常的时候抛出 error
 * 其余可预知异常统统 fail
 *
 * @author chaoshen
 */

@Data
public class Rs<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer code;

    private String message;

    private T data;

    public Rs() {

    }

    public Rs(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static Rs ok() {
        return new Rs(200, "success");
    }


    public static <T> Rs ok(T data) {
        Rs result = Rs.ok();
        result.data = data;
        return result;
    }

    public static <T> Rs ok(String message, T data) {
        Rs result = new Rs(200, message);
        result.data = data;
        return result;
    }

    public static Rs fail(String message) {
        return new Rs(400, message);
    }

    public static Rs fail(Object object) {
        Rs result = new Rs(400, "操作失败");
        result.setData(object);
        return result;
    }

    public static Rs error(String message) {
        return new Rs(500, message);
    }

    public static void main(String[] args) {
        System.out.println(JSONObject.toJSONString(Rs.ok("hello")));
    }
}
