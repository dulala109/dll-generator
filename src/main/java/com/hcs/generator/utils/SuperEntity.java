package com.hcs.generator.utils;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 基础实体类
 *
 * @author chaoshen
 * @data 2020/4/30 0030
 */
@Data
public class SuperEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "id不能为空")
    protected String id;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date createTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    protected Date updateTime;

    protected Integer isDelete;

    protected String operator;

}
